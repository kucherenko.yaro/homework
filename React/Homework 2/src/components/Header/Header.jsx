import React from 'react';
import Basket from "./Basket"
import FavoriteCards from "./FavoritCards"
import "./Header.scss"
import PropTypes from "prop-types";


const Header = ({favoriteCardSum,basketCardSum}) => {

    return (
        <header className="header">
            <a className="header__logo" href="#">Game Store</a>
            <div className="header__description">
                <div className="header__favorite">
                    <FavoriteCards />
                    <span className="header__cart-number">{favoriteCardSum}</span>
                </div>
                <div className="header__basket">
                    <Basket />
                    <span className="header__cart-number">{basketCardSum}</span>
                </div>
            </div>
        </header>
    );
};

Header.propTypes = {
    favoriteCardSum: PropTypes.number,
    basketCardSum: PropTypes.number,
}

export default Header;