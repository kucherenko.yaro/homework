import React from 'react';
import { AiOutlineStar, AiFillStar } from "react-icons/ai"
import {useState} from "react";
import "./GameItem.scss"
import PropTypes from "prop-types";

const StarIcon = ({addedCards, remoteCards}) => {

    const [addFavorites,setAddFavorites] = useState(false)
    const [notFavorites,setNotFavorites] = useState(true)

    return (
        <div className="star-icon">
            {notFavorites && <AiOutlineStar fontSize={26} onClick={() => {
                addedCards()
                setAddFavorites(true)
                setNotFavorites(false)
            }}/>}
            {addFavorites && <AiFillStar fontSize={26} onClick={() => {
                remoteCards()
                setNotFavorites(true)
                setAddFavorites(false)
            }}/>}
        </div>
    );
};

StarIcon.propTypes = {
    addedCards: PropTypes.func,
    remoteCards: PropTypes.func,
}

export default StarIcon;