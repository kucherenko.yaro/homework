import React from 'react';
import Button from "../../Button";
// import StarIcon from "./StarIcon/StarIcon";
import PropTypes from "prop-types"
import "./GameItem.scss"
import {AiFillStar, AiOutlineStar} from "react-icons/ai";
import {useState} from "react";


const GameItem = ({ cardProps, isOpenModal, addToCard,addedCards,removeCards}) => {
    const {title, article, price, image} = cardProps

    const favoritesLocalStorage = JSON.parse(localStorage.getItem("favorite"))
    const isFavorite = Boolean(favoritesLocalStorage?.find(favorite => favorite.article === article))


    const [addFavorites,setAddFavorites] = useState(!isFavorite ? false : true)
    const [notFavorites,setNotFavorites] = useState(isFavorite ? false : true)


    return (
        <div className="game__item">
            <img src={image} alt={title} width={320} height={180}/>
            <div className="game__item-details">
                <div className="game__item-header">
                    <h3 className="game__item-title">{title}</h3>
                    <div className="star-icon">
                        {notFavorites && <AiOutlineStar fontSize={26} onClick={() => {
                            addedCards()
                            setAddFavorites(true)
                            setNotFavorites(false)
                        }}/>}
                        {addFavorites && <AiFillStar fontSize={26} onClick={() => {
                            removeCards()
                            setNotFavorites(true)
                            setAddFavorites(false)
                        }}/>}
                    </div>
                </div>
                <span className="game__item-article">Артикул: {article}</span>
                <div className="game__item-description">
                    <p className="game__item-price">{price} грн.</p>
                    <Button children="Add game" onClick={() => {isOpenModal(); addToCard()}} />
                </div>
            </div>
        </div>
    );
};

GameItem.propTypes = {
    cardProps: PropTypes.object.isRequired,
    isOpenModal: PropTypes.func.isRequired,
    addToCard: PropTypes.func.isRequired,
    addedCards: PropTypes.func.isRequired,
    removeCards: PropTypes.func.isRequired,
}

export default GameItem;


