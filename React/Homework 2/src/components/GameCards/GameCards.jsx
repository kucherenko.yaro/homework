import React from 'react';
import {useState, useEffect} from "react";
import {sendRequest} from "../../helpers"
import GameItem from "./GameItem";
import Modal from "../Modal";
import "./GameCards.scss"
import PropTypes from "prop-types";

const GameCards = ({addedFavorite,removeFavorite,addThisCard}) => {
    const [modalOpen, setModalOpen] = useState(false)

    const [games, setGames] = useState([])
    const [selectedProduct,setSelectedProduct] = useState(null)


    useEffect(() => {
        sendRequest("./data.json")
            .then(({games}) => {
                setGames(games)
            })
    },[])


    const renderCardGames = games.map(card =>
        <GameItem
            cardProps={card}
            isOpenModal={() => setModalOpen(true)}
            key={card.article}
            addedCards={() => addedFavorite(card)}
            removeCards={() => removeFavorite(card)}
            addToCard={() => setSelectedProduct(card)}
        />
    )


    return (
        <div className="cards__wrap">
            {renderCardGames}

            {modalOpen &&
                <Modal
                    header="Подтвердите добавление в корзину"
                    text={selectedProduct.title}
                    closeModal={() => setModalOpen(false)}
                    onClick={() => addThisCard(selectedProduct)}
                />
            }
        </div>
    );
};

GameCards.propTypes = {
    addedFavorite: PropTypes.func,
    remoteFavorite: PropTypes.func,
    addInfoCard: PropTypes.func,
    addThisCard: PropTypes.func,
}

export default GameCards;