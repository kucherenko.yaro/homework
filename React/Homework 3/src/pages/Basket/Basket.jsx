import React from 'react';
import "./Basket.scss"
import Modal from "../../components/Modal";
import {useState} from "react";
import BasketCard from "./BasketCard";

const Basket = ({cards,deleteCard}) => {
    const [modalOpen, setModalOpen] = useState(false)

    const [cardItem, setCardItem] = useState([])


    localStorage.getItem("basket")

    const renderCard = cards.map(card => <BasketCard key={card.article}
                                                     cardProps={card}
                                                     isOpenModal={() => setModalOpen(true)}
                                                     onclick={() => setCardItem(card)}/>)


    return (
        <div className="basket__wrap">
            <header className="basket__header">Корзина</header>
            <div className="basket__items">
                {renderCard}
            </div>


            {modalOpen &&
                <Modal
                    header="Подтвердите удаления"
                    text="Вы уверенны что хотите удалить товар с корзины?"
                    closeModal={() => setModalOpen(false)}
                    onClick={() => deleteCard(cardItem)}
                />
            }
        </div>
    );
};

export default Basket;