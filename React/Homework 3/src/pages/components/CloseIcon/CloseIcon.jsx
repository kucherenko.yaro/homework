import React from 'react';
import { IoCloseSharp } from "react-icons/io5"

const CloseIcon = ({onClick}) => {
    return (
        <div className="close-card">
            <IoCloseSharp fontSize={33} onClick={onClick}/>
        </div>
    );
};

export default CloseIcon;