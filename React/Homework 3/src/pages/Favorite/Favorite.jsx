import React from 'react';
import "./Favorite.scss"
import FavoriteCard from "./FavoriteCard";

const Favorite = ({cards,showInfoCard}) => {

    const renderCard = cards.map(card => <FavoriteCard key={card.article} cardProps={card} showInfoCard={() => showInfoCard(card)}/>)

    return (
        <div className="favorite__wrap">
            <header className="favorite__header">Избранное</header>
            <div className="favorite__items">
                {renderCard}
            </div>
        </div>
    );
};

export default Favorite;