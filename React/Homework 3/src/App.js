import React from 'react';
import {useState, useEffect} from "react";
import Header from "./components/Header";
import GameCards from "./components/GameCards";
import Basket from "./pages/Basket";
import Favorite from "./pages/Favorite";
import { Routes, Route } from 'react-router-dom';
import "./App.scss"


const App = () => {
    const [basketCount, setBasketCount] = useState(JSON.parse(localStorage.getItem("basketCount")) || 0)
    const [favoriteCount, setFavoriteCount] = useState(JSON.parse(localStorage.getItem("favCount")) || 0)
    const [routeFavorite,setRouteFavorite] = useState(JSON.parse(localStorage.getItem("favorite")) || [])
    const [routeBasket,setRouteBasket] = useState(JSON.parse(localStorage.getItem("basket")) || [])


    useEffect(() => {
        localStorage.setItem("favorite",JSON.stringify(routeFavorite))
        localStorage.setItem("favCount",JSON.stringify(favoriteCount))
        localStorage.setItem("basket",JSON.stringify(routeBasket))
        localStorage.setItem("basketCount",JSON.stringify(basketCount))
    },[routeFavorite,favoriteCount, routeBasket,  basketCount])


    const increaseFavorite = (card) => {
        setFavoriteCount(favoriteCount + 1)
        setRouteFavorite([...routeFavorite, card])
    }

    const decreaseFavorite = (card) => {
        const {article} = card
        setFavoriteCount(favoriteCount - 1)
        setRouteFavorite(routeFavorite.filter(el => el.article !== article))
    }

    const addCardItem = (card) => {
        const cardId = routeBasket.find(el => el.article === card.article)
        if (cardId) {
            alert(`Игра ${card.title} уже добавлена в корзину`)
        } else {
            setBasketCount(basketCount + 1)
            setRouteBasket([...routeBasket,card])
        }
    }

    const deleteCard = ({article}) => {
        setBasketCount(basketCount - 1)
        setRouteBasket(routeBasket.filter(el => el.article !== article))
    }

    return (
        <div className="wrapper">
            <Header favoriteCardSum={favoriteCount} basketCardSum={basketCount}/>
            <Routes>
                <Route index element={<GameCards
                    addedFavorite={increaseFavorite}
                    removeFavorite={decreaseFavorite}
                    addThisCard={addCardItem}
                    />} />

                <Route path="/basket" element={<Basket cards={routeBasket} deleteCard={deleteCard}/>} />

                <Route path="/favorite" element={<Favorite cards={routeFavorite} showInfoCard={decreaseFavorite}/>} />
            </Routes>
        </div>
    );
};

export default App;