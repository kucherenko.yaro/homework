import React from 'react';
import "./Button.scss"
import PropTypes from "prop-types";
import GameItem from "../GameCards/GameItem";

const Button = ({children, onClick}) => {

    return (
        <button className="button" onClick={onClick}>{children}</button>
    );
};


Button.propTypes = {
    children: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
}

export default Button;
