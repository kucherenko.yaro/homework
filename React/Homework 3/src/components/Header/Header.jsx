import React from 'react';
import Basket from "./Basket"
import FavoriteCards from "./FavoritCards"
import "./Header.scss"
import PropTypes from "prop-types";
import HomePage from "./HomePage/HomePage";
import {Link} from "react-router-dom";


const Header = ({favoriteCardSum,basketCardSum}) => {

    return (
        <header className="header">
            <Link to="/" className="header__logo">Game Store</Link>
            <div className="header__description">
                <Link to="/" className="header__homepage">
                    <HomePage />
                </Link>
                <Link to="/favorite" className="header__favorite">
                    <FavoriteCards />
                    <span className="header__cart-number">{favoriteCardSum}</span>
                </Link>
                <Link to="/basket" className="header__basket">
                    <Basket />
                    <span className="header__cart-number">{basketCardSum}</span>
                </Link>
            </div>
        </header>
    );
};


export default Header;