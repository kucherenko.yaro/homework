import React from 'react';
import Button from "../Button";

import "./Modal.scss"
import PropTypes from "prop-types";


const Modal = ({header, closeModal, text, onClick}) => {

    return (
        <div className="modal" onClick={closeModal}>
            <div className="modal__content" onClick={(e) => e.stopPropagation()}>
                <header className="modal__header">
                    {header}
                    <span className="modal__icon-delete" onClick={closeModal}></span>
                </header>
                <div className="main__content">
                    {text}
                </div>
                <div className="button__modals">
                    <Button
                        children="Ok"
                        backgroundColor="#3c3c64"
                        onClick={() => {onClick(); closeModal()}}
                    />
                    <Button
                        children="Cancel"
                        backgroundColor="#2b2b46"
                        onClick={closeModal}
                    />
                </div>
            </div>
        </div>
    );
};

Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    closeModal: PropTypes.func,
    addThisCard: PropTypes.func,
}

export default Modal;
