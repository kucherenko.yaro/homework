import React, {Component} from 'react';
import Button from "./components/Button";

import "./App.scss"
import Modal from "./components/Modal";

class App extends Component {
    state = {
        isModal1: false,
        isModal2: false
    }

    closeModal = () => {
        this.setState({
            isModal1: false,
            isModal2: false,
        });
    };

    render() {
        const {isModal1, isModal2} = this.state

        return (
            <div className="wrapper">
                <div className="button-wrap">
                    <Button
                        children="Open first modal"
                        backgroundColor="#2d2d57"
                        onClick={() => this.setState({isModal1: true})}
                    />
                    <Button
                        children="Open second modal"
                        backgroundColor="#3a3a46"
                        onClick={() => this.setState({isModal2: true})}
                    />
                </div>

                {isModal1 &&
                    <Modal
                        header="Open first modal"
                        text="You have opened a modal window one, welcome you!"
                        closeModal={this.closeModal}
                    />
                }
                {isModal2 &&
                    <Modal
                        header="Open second modal"
                        text="You have opened a modal window two.Close this window immediately!"
                        closeModal={this.closeModal}
                    />
                }
            </div>
        );
    }
}

export default App;
