import React, {Component} from 'react';
import Button from "../Button";

import "./Modal.scss"

class Modal extends Component {
    render() {
        const {header, closeModal, text} = this.props

        return (
            <div className="modal" onClick={() => closeModal()}>
                <div className="modal__content" onClick={(e) => e.stopPropagation()}>
                    <header className="modal__header">
                        {header}
                       <span className="modal__icon-delete" onClick={() => closeModal()}></span>
                    </header>
                    <div className="main__content">
                        {text}
                    </div>
                    <div className="button__modals">
                        <Button
                            children="Ok"
                            backgroundColor="#3c3c64"
                            onClick={() => alert("Вы нажали кнопку Ок")}
                        />
                        <Button
                            children="Cancel"
                            backgroundColor="#2b2b46"
                            onClick={() => closeModal()}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;
