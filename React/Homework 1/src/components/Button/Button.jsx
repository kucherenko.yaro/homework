import React, {Component} from "react";

import "./Button.scss"

class Button extends Component {
    render() {
        const {onClick, children, backgroundColor} = this.props

        return (
            <button className="button" style={{backgroundColor:backgroundColor}} onClick={onClick}>{children}</button>
        );
    }
}

export default Button;
