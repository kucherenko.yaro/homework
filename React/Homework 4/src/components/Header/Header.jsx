import React,{useEffect} from 'react';
import Basket from "./Basket"
import FavoriteCards from "./FavoritCards"
import "./Header.scss"
import HomePage from "./HomePage/HomePage";
import {Link} from "react-router-dom";
import { useSelector } from 'react-redux'


const Header = () => {
    const basketCount = useSelector(state => state.basket.routeBasket)
    const favoriteCount = useSelector(state => state.favorite.routeFavorite)

    useEffect(() => {
        localStorage.setItem("basketCount",JSON.stringify(basketCount))
        localStorage.setItem("favoriteCount",JSON.stringify(favoriteCount))
    },[basketCount,favoriteCount])


    return (
        <header className="header">
            <Link to="/" className="header__logo">Game Store</Link>
            <div className="header__description">
                <Link to="/" className="header__homepage">
                    <HomePage />
                </Link>
                <Link to="/favorite" className="header__favorite">
                    <FavoriteCards />
                    <span className="header__cart-number">{favoriteCount.length}</span>
                </Link>
                <Link to="/basket" className="header__basket">
                    <Basket />
                    <span className="header__cart-number">{basketCount.length}</span>
                </Link>
            </div>
        </header>
    );
};


export default Header;