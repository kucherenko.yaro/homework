import favoriteReducer, {increaseFavorite,decreaseFavorite} from "./favorite.reducer";
import todoReducer, {actionFetchCards} from "./todoSlice"
import basketReducer, {increaseBasket,decreaseBasket} from "./basket.reducer"
import modalReducer, {modalOpen,modalClose} from "./modal.reducer"

export {
    favoriteReducer,
    increaseFavorite,
    decreaseFavorite,
    todoReducer,
    actionFetchCards,
    basketReducer,
    increaseBasket,
    decreaseBasket,
    modalReducer,
    modalOpen,
    modalClose
}