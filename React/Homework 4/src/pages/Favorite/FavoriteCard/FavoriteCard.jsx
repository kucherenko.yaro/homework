import React from 'react';
import "./FavoriteCard.scss"
import StarIcon from "../../components/StarIcon";
import { useDispatch } from 'react-redux'
import {decreaseFavorite} from "../../../reducers";


const FavoriteCard = ({ cardProps}) => {
    const {title, article, price, image} = cardProps

    const dispatch = useDispatch()

    return (
        <div className="pages__item">
            <img src={image} alt={title} width={320} height={180}/>
            <div className="pages__item-details">
                <div className="pages__item-header">
                    <h3 className="pages__item-title">{title}</h3>
                    <span className="pages__item-article">Артикул: {article}</span>
                </div>
                <div className="pages__item-description">
                    <p className="pages__item-price">{price} грн.</p>
                    <StarIcon onClick={() => dispatch(decreaseFavorite(cardProps))} />
                </div>
            </div>
        </div>
    )
};


export default FavoriteCard;