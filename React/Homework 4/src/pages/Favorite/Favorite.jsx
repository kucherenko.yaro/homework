import React from 'react';
import "./Favorite.scss"
import FavoriteCard from "./FavoriteCard";
import { useSelector } from 'react-redux'
import {useEffect} from "react";

const Favorite = () => {
    const cardsFavorite = useSelector(state => state.favorite.routeFavorite)

    const renderCard = cardsFavorite.map(card => <FavoriteCard key={card.article} cardProps={card}/>)

    return (
        <div className="favorite__wrap">
            <header className="favorite__header">Избранное</header>
            <div className="favorite__items">
                {renderCard}
            </div>
        </div>
    );
};

export default Favorite;