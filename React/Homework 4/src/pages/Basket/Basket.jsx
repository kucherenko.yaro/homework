import React from 'react';
import "./Basket.scss"
import Modal from "../../components/Modal";
import {useState,useEffect} from "react";
import BasketCard from "./BasketCard";
import { useSelector,useDispatch } from 'react-redux'
import {decreaseBasket,modalOpen,modalClose} from "../../reducers"

const Basket = () => {

    const [cardItem, setCardItem] = useState([])

    const cards = useSelector(state => state.basket.routeBasket)
    const modal = useSelector(state => state.modal.isModal)
    const dispatch = useDispatch()

    const renderCard = cards.map(card => <BasketCard key={card.article} cardProps={card}
                                                     isOpenModal={() => dispatch(modalOpen())}
                                                     onclick={() => setCardItem(card)}/>)


    return (
        <div className="basket__wrap">
            <header className="basket__header">Корзина</header>
            <div className="basket__items">
                {renderCard}
            </div>

            {modal &&
                <Modal
                    header="Подтвердите удаления"
                    text="Вы уверенны что хотите удалить товар с корзины?"
                    closeModal={() => dispatch(modalClose())}
                    onClick={() => dispatch(decreaseBasket(cardItem))}
                />
            }
        </div>
    );
};

export default Basket;