import React from 'react';
import "./BasketCard.scss"
import CloseIcon from "../../components/CloseIcon";


const BasketCard = ({ cardProps, isOpenModal, onclick}) => {
    const {title, article, price, image} = cardProps

    return (
        <div className="pages__item">
            <img src={image} alt={title} width={320} height={180}/>
            <div className="pages__item-details">
                <div className="pages__item-header">
                    <h3 className="pages__item-title">{title}</h3>
                    <span className="pages__item-article">Артикул: {article}</span>
                </div>
                <div className="pages__item-description">
                    <p className="pages__item-price">{price} грн.</p>
                    <CloseIcon onClick={() => {isOpenModal(); onclick()}} />
                </div>
            </div>
        </div>
    )
};


export default BasketCard;