import React from 'react';
import Header from "./components/Header";
import Cards from "./components/Cards";
import Basket from "./pages/Basket";
import Favorite from "./pages/Favorite";
import { Routes, Route } from 'react-router-dom';
import "./App.scss"
import ToDoProvider from "./context";


const App = () => {

    return (
        <ToDoProvider>
            <div className="wrapper" data-testid="home-page">
                <Header />
                <Routes>
                    <Route index element={<Cards/>} />
                    <Route path="/basket" element={<Basket/>} />
                    <Route path="/favorite" element={<Favorite/>} />
                </Routes>
            </div>
        </ToDoProvider>
    );
};

export default App;