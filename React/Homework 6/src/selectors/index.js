import {basketSelector} from "./basket.selector"
import {favouriteSelector} from "./favourite.selector"
import {cardsSelector} from "./toDoCards.selector"
import {isModalSelector,isModalSubmitSelector,isModalPageFormSelector} from "./modal.selector"

export {basketSelector,favouriteSelector,cardsSelector,isModalSelector,isModalSubmitSelector,isModalPageFormSelector}