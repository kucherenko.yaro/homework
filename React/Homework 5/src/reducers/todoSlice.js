import { createSlice,createAsyncThunk } from '@reduxjs/toolkit'
import {sendRequest} from "../helpers"

const initialState = {
    todos: []
}

export const actionFetchCards = createAsyncThunk("todos/fetchCards",async () => {
    const response = await sendRequest("./data.json")
    return response
})

const todoSlice = createSlice({
    name: 'todos',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(actionFetchCards.fulfilled,(state,{payload})=> {
            state.todos = payload
        })
    }
})

export default todoSlice.reducer