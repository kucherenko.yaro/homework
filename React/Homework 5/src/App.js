import React from 'react';
import {useEffect} from "react";
import Header from "./components/Header";
import GameCards from "./components/GameCards";
import Basket from "./pages/Basket";
import Favorite from "./pages/Favorite";
import { Routes, Route } from 'react-router-dom';
import "./App.scss"
import {actionFetchCards} from "./reducers";
import {useDispatch} from "react-redux";


const App = () => {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(actionFetchCards());
    },[dispatch])


    return (
        <div className="wrapper">
            <Header />
            <Routes>
                <Route index element={<GameCards/>} />
                <Route path="/basket" element={<Basket/>} />
                <Route path="/favorite" element={<Favorite/>} />
            </Routes>
        </div>
    );
};

export default App;