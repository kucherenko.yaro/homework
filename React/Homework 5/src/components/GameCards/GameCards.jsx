import React from 'react';
import {useState} from "react";
import GameItem from "./GameItem";
import Modal from "../Modal";
import "./GameCards.scss"
import { useSelector,useDispatch } from 'react-redux'
import {increaseBasket,modalOpen,modalClose} from "../../reducers"



const GameCards = () => {
    const [selectedProduct,setSelectedProduct] = useState([])

    const dispatch = useDispatch()

    const cards = useSelector(state => state.todos.todos.cards)
    const modal = useSelector(state => state.modal.isModal)


    return (
        <div className="cards__wrap">
            {!cards ? "loading" : cards.map(card => <GameItem
                    cardProps={card}
                    isOpenModal={() => dispatch(modalOpen())}
                    key={card.article}
                    addToCard={() => setSelectedProduct(card)}
                />
            )}

            {modal &&
                <Modal
                    header="Подтвердите добавление"
                    text={`Добавить ${selectedProduct.title} в корзину?`}
                    closeModal={() => dispatch(modalClose())}
                    onClick={() => dispatch(increaseBasket(selectedProduct))}
                />
            }
        </div>
    );
};

export default GameCards;