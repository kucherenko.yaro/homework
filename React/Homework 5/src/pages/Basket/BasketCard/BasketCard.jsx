import React from 'react';
import "./BasketCard.scss"
import CloseIcon from "../../components/CloseIcon";


const BasketCard = ({ cardProps, isOpenModal, onclick}) => {
    const {title, article, price, image} = cardProps

    return (
        <div className="basket__item">
            <img src={image} alt={title} width={320} height={180}/>
            <div className="basket__item-details">
                <div className="basket__item-header">
                    <h3 className="basket__item-title">{title}</h3>
                    <span className="basket__item-article">Артикул: {article}</span>
                </div>
                <div className="basket__item-description">
                    <p className="basket__item-price">{price} грн.</p>
                    <div className="close-card__basket">
                        <CloseIcon onClick={() => {isOpenModal(); onclick()}} />
                    </div>
                </div>
            </div>
        </div>
    )
};


export default BasketCard;