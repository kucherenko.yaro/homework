import React, {useEffect} from 'react';
import "./Basket.scss"
import Modal from "../../components/Modal";
import PageForm from "../../components/Form/PageForm";
import BasketCard from "./BasketCard";
import Button from "../../components/Button";
import ModalSubmit from "../../components/Form/components/ModalSubmit";

import {useState} from "react";
import { useSelector,useDispatch } from 'react-redux'
import {decreaseBasket, modalOpen, modalClose, formOpen, modalSubmitClose,clearItems} from "../../reducers"
import {BiCartAlt} from "react-icons/bi";


const Basket = () => {

    const [cardItem, setCardItem] = useState([])

    const modalFinishSubmit = useSelector(state => state.modal.modalSubmit)
    const cards = useSelector(state => state.basket.routeBasket)
    const modal = useSelector(state => state.modal.isModal)
    const formPage = useSelector(state => state.modal.pageForm)
    const dispatch = useDispatch()


    const renderCard = cards.map(card => <BasketCard key={card.article} cardProps={card}
                                                     isOpenModal={() => dispatch(modalOpen())}
                                                     onclick={() => setCardItem(card)}/>
    )


    return (
        <div className="basket__wrap">
            <header className="basket__header">
                <p>Корзина</p>
                {cards.length > 0 ? <div className="basket__button">
                                        <Button children="Оформить заказ" className="button" onClick={() => dispatch(formOpen())}/>
                                    </div>
                                  : null}
            </header>

            <div className="basket__items">
                {cards.length < 1 ? <div className="empty-basket"><BiCartAlt fontSize={180} /></div> : null}

                {renderCard}
            </div>

            {modal &&
                <Modal
                    header="Подтвердите удаления"
                    text={`Вы уверенны что хотите удалить ${cardItem.title} с корзины?`}
                    closeModal={() => dispatch(modalClose())}
                    onClick={() => dispatch(decreaseBasket(cardItem))}
                />
            }

            {formPage && <PageForm/>}

            {modalFinishSubmit &&
                <ModalSubmit
                    header="Спасибо за покупку!"
                    text="Данные оправлены в обработку, спасибо за Ваш заказ."
                    closeModal={() => dispatch(modalSubmitClose())}
                    onClick={() => dispatch(clearItems())}
                />
            }
        </div>
    );
};

export default Basket;