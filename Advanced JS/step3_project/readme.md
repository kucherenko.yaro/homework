

## Кучеренко Ярослав

**Выполнял задания:**

- Создание класса **Modal** по созданию модального окна.
- Создание класса **allDoctorsButton** по созданию кнопок с докторами при нажатии которых всплывает форма для каждого врача.
- Создание класса **Visit** по созданию формы с основными полями для всех докторов.
- Создание дочерних классов **VisitCardiologist, VisitDentist, VisitTherapist** по созданию дополнительных полей для каждого врача.
- Создание метода по поиску всех полей формы.
- Создание класса **UserCard** с дочерними классами **UserCardTherapist, UserCardDentist, UserCardCardiologist** с методами по созданию карточки и вывода всей информации про клиента на екран.
- Создание запроса на **создание** и **удаление** карточки.
- Создание метода на **добавления и удаления дополнительной информации** про клиента при нажатии.
 


## Головко Алина

**Выполняла задания:**
- HTML-структура сайта и css-стили.
- Создание класса **userLogin** для авторизации.
- Сохранение и вывод токена.
- Создание класса **EditForm** и дочерних классов **EditFormTherapist**, **EditFormDentist**, **EditFormCardiologist** для вызова формы редактирования карточек.
- Проверка данных при входе и вывод существующих карточек на сайт после авторизации, добавление/удаление надписи *No items have been added*.
- Основной sendRequest, запрос на токен и на поиск карточек по айди.
- Создание функции **showCards** для проверки всех карточек и вывода их на сайт.


## Рожко Станислав

**Выполняла задания:**
- Фильтрация