/**    Теоретичне питання
 Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.*/

// Ajax - це набір методів для створення асинхронного коду.За допомогою Ajax можна відправляти дані на сервер і отримувати дані з сервера
//  не впливаючи на відображення сторінки.

/**    Завдання
 Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

 ***  Технічні вимоги:
 Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни

 Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі.
 Список персонажів можна отримати з властивості characters.

 Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані.
 Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).

 Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

 Необов'язкове завдання підвищеної складності
 Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку.
 Бажано знайти варіант на чистому CSS без використання JavaScript.*/


const API = "https://ajax.test-danit.com/api/swapi/films"

const posts = document.querySelector('#posts')

const renderPostFilms = (url) => {
     fetch(url)
        .then(response => response.json())
        .then(data => {
            console.log(data);

            data.forEach(({episodeId, name, openingCrawl, characters}) => {

                posts.insertAdjacentHTML("afterbegin",`
                <div class="film-content">
                    <p class="film-episode">Episode: ${ episodeId }</p>
					<p class="film-title">${ name }</p>
					<div class="actors"></div>
					<p class="film-info">${ openingCrawl }</p>
				</div>
                `)

                const sectionActors = document.querySelector(".actors")

                const requestActors = characters.map(url => fetch( url ))
                Promise.all(requestActors)
                    .then( responses => {
                        const results = responses.map( response => response.json() );
                        Promise.all( results ).then( results => {
                            results.forEach(({name}) => {
                                sectionActors.insertAdjacentHTML("afterbegin", `
                                <span>${name},</span>
                                `)
                                console.log(name);
                            })
                        } )
                    })

            })
        })
}

renderPostFilms(API)

