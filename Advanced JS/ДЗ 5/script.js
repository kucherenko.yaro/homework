const API = "https://ajax.test-danit.com/api/json/";

const cardsWrap = document.querySelector(".cards_wrap");


class Card {
    constructor(title, body, userName, userEmail, userId, authorId, postId) {
        this.title = title;
        this.body = body;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userId = userId;
        this.authorId = authorId;
        this.postId = postId;
    }

    render () {
        const cardItem = document.createElement("div");
        cardItem.className = 'user-card';
        cardItem.id = `user-card-${this.postId}`;
        cardsWrap.append(cardItem);
        const button = document.createElement('button');
        button.className = 'button'
        button.innerText = 'DELETE';
        cardItem.append(button);
        cardItem.insertAdjacentHTML("beforeend", `
                        <header>
                             <p class="username">${this.userName}</p>
                        </header>
                        <a class="email">${this.userEmail}</a>
                        <div class="user-posts">
                             <div class="title">${this.title}</div>
                             <div class="desc">${this.body}</div>
                         </div>
                    `);


        button.addEventListener('click', (event) => {

            const postId = event.target.closest('.user-card').getAttribute('id').slice(10);
            console.log(postId);

            deletePost(postId)
                .then(() => {
                    document.querySelector(`#user-card-${postId}`).remove()
                })

        })
    }
}

const sendRequest = async (entity, method = "GET") => {
    return await fetch(`${API}${entity}`, {
        method,
    }).then(response => {
        if(response.ok){
            if(method !== 'DELETE'){
                return response.json()
            } else {
                return response
            }
        } else {
            return new Error('Что-то пошло не так');
        }
    })
}

const getUsers = () => sendRequest("users");
const getPosts = () => sendRequest("posts");
const deletePost = (postId) => sendRequest(`posts/${postId}`, 'DELETE');
//
// getUsers()
//     .then(users => {
//         users.forEach(({name, email, id}) => {
//             getPosts(id)
//                 .then(posts => {
//                     posts.forEach(({title, body, userId, id}) => {
//
//                         const card = new Card(title, body, name, email, id, userId, id)
//                         card.render()
//                     })
//                 })
//         });
//     })

getPosts()
    .then(posts => {
        posts.forEach(({title, body, userId, id}) => {
            getUsers()
                .then(users => {
                    users.forEach(({name, email, id}) => {
                        if (id === userId) {
                            const card = new Card(title, body, name, email, id, userId, id);
                            card.render();
                        }

                    })
                })
        })
    })


