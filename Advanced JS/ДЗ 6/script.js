
const IP = "https://api.ipify.org/?format=json"
const SERVIS = "http://ip-api.com/"


const button = document.querySelector(".button")

const sendRequest = (url) => {
    return fetch(url)
       .then(response => {
           if(response.ok){
               return response.json()
           } else {
               return new Error('Что-то пошло не так');
           }
       })
}


button.addEventListener("click", async(event) => {
    event.preventDefault()

    const userIp = await sendRequest(IP)
    const ip = userIp.ip;

    const userInfo = await sendRequest(`${SERVIS}json/${ip}`)
    const  {city, country, timezone, regionName} = userInfo;

    button.insertAdjacentHTML("afterend", `
            <div class="infoId">
                <p>City:  ${city}</p>
                <p>Country:  ${country}</p>
                <p>Timezone:  ${timezone}</p>
                <p>Region:  ${regionName}</p>
               
            </div>
        `)
} )


