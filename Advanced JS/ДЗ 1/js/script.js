/**  Теоретичне питання
1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
2. Для чого потрібно викликати super() у конструкторі класу-нащадка?*/

/** Відповідь*/

// 1. Якщо в нас є об`єкт з деякими свойствами і ми хочемо створити ще декілька об`єктів видозмінені,
// то нам не потрібно копіювати всі методи,а просто создать об`єкт на основі першого.
// 2. super() ми викликаємо для того,щоб можна було використовувати функції які належать батьківському класу.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    set name(value) {
        this._name = value;
    }

    set age(value) {
        this._age = value;
    }

    set salary(value) {
        this._salary = value;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

}


class Programmer extends Employee {
    constructor(name, age, salary, lang ) {
        super(name, age, salary);
        this.lang = lang;
    }

    set salary(value) {
        this._salary = value;
    }

    get salary() {
        if (this._salary){
           return this._salary * 3
        }
    }
}


const person1 = new Employee("Vlad", 30, 40000)
const person2 = new Programmer("Yaroslav", 23, 50000,)
const person3 = new Programmer("Ivan", 40, 150000,["Ukrainian", "Italian", "Spanish"])

console.log(person1);
console.log(person2);
console.log(person3);
