/**Теоретичне питання
 1. Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.*/

// try...catch використовують для того щоб коли виникала якась помилка то вся програма  не летілаБа код і далі продовжував
    //     праювати,наприклад коли користувач не ввів якісь свої дані там де вони необхідні,то щоб
    //     ведавало помилку, і код не зупинявся.

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


let list = document.querySelector("#root")


books.forEach(elem => {
    try {
        if (elem.name && elem.author && elem.price){
            list.insertAdjacentHTML("beforeend", `
                    <ul>
                        <li>${elem.author}</li>
                        <li>${elem.name}</li>
                        <li>${elem.price}</li>
                    </ul>
               `)
        } else {
            throw new SyntaxError(`Елемент : ${elem.name} відсутній`)
        }
    } catch (err) {

        console.log(err)
    }
})



// books.forEach(elem => {
//
//     list.insertAdjacentHTML("beforeend", `
//     <ul>
//         <li>${elem.author}</li>
//         <li>${elem.name}</li>
//         <li>${elem.price}</li>
//     </ul>
//     `)
//     console.log(a);
// })
