// ## Теоретичні питання
//
// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// 2. Які засоби оголошення функцій ви знаєте?
// 3. Що таке hoisting, як він працює для змінних та функцій?
//

// Відповідь
// 1.Екранування в програмуванні потрібні для того щоб використовувати спеціальні символи в програмуванні,наприклад кавички,слеш.
// 2.Функції можна оголосити трьома способами:function declaration,function expression це функція яка зеписується в змінну,і стрілочна функція
// 3.hoisting - це механізм в script при якому змінні і оголошення функцій здвигаються вверх перед тим як код буде виконано

// ## Завдання
//
// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//
// #### Технічні вимоги:
//
//  - Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// 1. При виклику функція повинна запитати дату народження (текст у форматі `dd.mm.yyyy`) і зберегти її в полі `birthday`.
// 2. Створити метод `getAge()` який повертатиме скільки користувачеві років.
// 3. Створити метод `getPassword()`, який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`.
// - Вивести в консоль результат роботи функції `createNewUser()`, а також функцій `getAge()` та `getPassword()` створеного об'єкта.


// const createNewUser = () => {
//     let name = prompt('Enter your name')
//     let surName = prompt('Enter your last name ')
//     let age = prompt('Enter your age in format dd.mm.yyyy')
//     let add = age.split(".")
//
//
//     const newUser = {
//         firstName: name,
//         lastName: surName,
//         birthday: age,
//
//         // Не вийшло вирахувати скільки років,перепробував багато варіантів але так і не получилося(
//         //можливо підскажете як можна було б записати
//
//         // getAge () {
//         //     let nowData = new Date();
//         //     // let todey = new Date(nowData.getFullYear(), nowData.getMonth(), nowData.getDay());
//         //     let userBirthday = nowData.getFullYear() - add;
//         //     return userBirthday
//         //
//         // },
//         getPassword () {
//             return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + add[2]
//         }
//     }
//     return newUser
// }
//
//  let user = createNewUser();
//
// // console.log(user.getAge());
// console.log(user.getPassword());

const fName = prompt("Enter your name","MAX");
const surname = prompt("Enter your surname","Yevtushenko");
const date = prompt("Enter your date of birth DD.MM.YYYY","23.02.1994");

const showLogin = (name,surname,date) => {
    const datenow = new Date().toLocaleString().split(", ")[0].split(".");
    const formatedBirth = date.split(".");
    return {
        name,
        surname,
        getLogin: () => console.log(name[0].toLowerCase() + surname.toLowerCase()),
        getPassword: () => console.log(name[0] + surname.toLowerCase() + +datenow[2]),
        getAge: () => {
            if(+formatedBirth[1] > +datenow[1]){
                console.log( datenow[2] - formatedBirth[2] - 1)
            }else if(+formatedBirth[1] === +datenow[1] && +formatedBirth[0] > +datenow[0]){
                console.log(datenow[2] - formatedBirth[2] - 1)
            }else{
                console.log( datenow[2] - formatedBirth[2])
            }
        }
    }
}


