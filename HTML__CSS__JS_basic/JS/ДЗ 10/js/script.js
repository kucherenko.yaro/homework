/**## Завдання

Реалізувати перемикання вкладок (таби) на чистому Javascript.

#### Технічні вимоги:
    - У папці `tabs` лежить розмітка для вкладок.
    Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки.
    При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися.
    При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.*/

const tabBtn = document.querySelectorAll(".tabs-title")
const textBtn = document.querySelectorAll(".tabs-item")


const btnList = () => {
    tabBtn.forEach(item => {
        item.addEventListener("click", () => {
            let currenBtn = item;
            let btnId = currenBtn.getAttribute("data-tab")
            let currenTab = document.querySelector(btnId)

            textBtn.forEach(item => {
                item.classList.remove("active")
            })
            tabBtn.forEach(item => {
                item.classList.remove("active")
            })

            currenBtn.classList.add("active")
            currenTab.classList.add("active")
        })
    })
}
btnList()
