
//       Теоретичні питання
//  1. Які існують типи даних у Javascript?
//  2. У чому різниця між == і ===?
 // 3. Що таке оператор?

// // Відповідь
// 1.Існує 8 типів даних:
// undefined,Null,Boolean,number,BigInt,string,symbol.BigInt
// 2.== це оператор порівняння,він не враховує тип даних
//   === це оператор порівняння який враховує тип даних
// 3.Це символ який повідомляє транслятору яку треба виконати операці,наприклад * / >  <


//Технічні вимоги:
// Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
// Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням:
// Are you sure you want to continue? і кнопками Ok, Cancel.
// Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача.
// Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
// Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
// Після введення даних додати перевірку їхньої коректності.
// Якщо користувач не ввів ім'я, або при введенні віку вказав не число - запитати ім'я та вік наново
// (при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).



let name = prompt("Введіть ваше ім'я")
while (!Boolean(name) || !isNaN(Number(name))) {
    name = prompt("Введіть ваше ім'я")
}
let age = +prompt("Введіть ваш вік")
while (isNaN(age)  || age < 0 || age > 100) {
    age = +prompt("Введіть ваш вік")
}
if(age < 18) {
     alert('You are not allowed to visit this website')
}else if(age >= 23) {
    confirm('Welcome, ' + name)
}else if(age >= 18 && age <= 22) {
    let answer = confirm('Are you sure you want to continue?')
    if(answer === true){
        alert('Welcome, ' + name)
    }else {
        alert('You are not allowed to visit this website')
    }
}
console.log(name)
console.log(age, typeof age)