// Теоретичні питання
// 1. Як можна оголосити змінну у Javascript?
// 2. У чому різниця між функцією prompt та функцією confirm?
// 3. Що таке неявне перетворення типів? Наведіть один приклад.

//   Відповідь
// 1.Змінну можна оголосити вказавши let , const або застарілий варіант var.
// let назва = (вміст змінної)

// 2.У функції prompt з'явиться віконце для запису інформації про клієнта, а у функції confirm випливе вікно із запитанням на яке буде кнопка "ОК" або "ОТМЕНА",тобто в консолі виведе true або false.

// 3.Неявне перетворення типів це процес переведення одного типу на інший,наприклад строку в число
// let age = ("10")
// console.log(age)
// console.log(typeof age)

// age = Number(age)
// console.log(age)
// console.log(typeof age)


// Завдання 1

let name = ('Ярослав');
let admin = ('Ярослав');

console.log(name)
console.log(admin)

// Завдання 2

let days = 5
let daysToSeconds = (24 * 60 * 60)

console.log(days * daysToSeconds)

// Завдання 3

var age = +prompt("How old are you?");

console.log(age);
console.log(typeof age)
