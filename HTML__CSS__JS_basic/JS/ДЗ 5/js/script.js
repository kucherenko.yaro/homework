// ## Теоретичні питання
// 1. Опишіть своїми словами, що таке метод об'єкту
// 2. Який тип даних може мати значення властивості об'єкта?
// 3. Об'єкт це посилальний тип даних. Що означає це поняття?

// Відповідь
//  1.Метод об'єкту це функція яка знаходиться в об'єкті
//  2.Будь-який тип данихю
//  3.

// ## Завдання
//
// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//
// #### Технічні вимоги:
//
//  - Написати функцію `createNewUser()`, яка буде створювати та повертати об'єкт `newUser`.
// - При виклику функція повинна запитати ім'я та прізвище.
// - Використовуючи дані, введені юзером, створити об'єкт `newUser` з властивостями `firstName` та `lastName`.
// - Додати в об'єкт `newUser` метод `getLogin()`, який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, `Ivan Kravchenko → ikravchenko`).
// - Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію `getLogin()`. Вивести у консоль результат виконання функції.
//
// #### Необов'язкове завдання підвищеної складності
//
// - Зробити так, щоб властивості `firstName` та `lastName` не можна було змінювати напряму. Створити функції-сеттери `setFirstName()` та `setLastName()`, які дозволять змінити дані властивості.

const createNewUser = () => {
    let name = prompt('Введіть ваше firstName ')
    let surName = prompt('Введіть ваше lastName ')

    const newUser = {
        firstName: name,
        lastName: surName,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase()
        }
    }
    return newUser
}

let user = createNewUser()
console.log(user.getLogin());



// const createNewUser = () => {
//     const user = Object.create({}, {
//         firstName: {
//             value: "Yaroslav",
//             writable: false,
//             configurable: true,
//         },
//         lastName: {
//             value: "Kucherenko",
//             writable: false,
//             configurable: true,
//         },
//         // setFirstName() {
//         //     Object.defineProperties(this,firstName,{
//         //         writable: true,
//         //     })
//         // },
//         // setLastName() {
//         //     Object.defineProperties(this,lastName,{
//         //         writable: true,
//         //     })
//         // },
//         getLogin() {
//             let result = firstName[0] + lastName;
//             return result.toLowerCase();
//         },
//     })
// }
// const newUser = createNewUser;
//
// console.log(newUser);
//
// console.log(newUser.getLogin());
//
