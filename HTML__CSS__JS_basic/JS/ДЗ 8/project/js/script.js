/**Теоретичні питання
 * 1.Опишіть своїми словами що таке Document Object Model (DOM)
 2 Яка різниця між властивостями HTML-елементів innerHTML та innerText?
 3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?*/

// 1.Document Object Model це представлення HTML-документа у вигляді тегів
// 2. innerHTML дозволяє отримати вміст елементи разом з тегами у вигляді строки  а innerText дозволяє отримати зміст елемента у вигляді чистого тесту  без тегів
// 3.Краще звернутися до елемента за допомогою querySelector ,цей спосіб є абсолютно універсальний.

/**Завдання
 Код для завдань лежить в папці project.

 Знайти всі параграфи на сторінці та встановити колір фону #ff0000

 Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
 Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

 Встановіть в якості контента елемента з класом testParagraph наступний параграф -

 This is a paragraph

 Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
 Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.*/



/** Знайти всі параграфи на сторінці та встановити колір фону #ff0000*/
const emptyElem = document.getElementsByTagName("p")
for (let emptyElemElement of emptyElem) {
    emptyElemElement.style.backgroundColor = "#ff0000"
}
/** Знайти елемент із id="optionsList". Вивести у консоль.*/
const optionsList = document.getElementById("optionsList")
console.log(optionsList);

/**Знайти батьківський елемент та вивести в консоль.*/
const conteiner = document.querySelector(".section-options")
console.log(conteiner);

/**Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.*/
console.log( conteiner.childNodes);
console.log(typeof conteiner.childNodes);

/** Встановіть в якості контента елемента з класом testParagraph наступний параграф -*/

let testParagraph = document.getElementById("testParagraph")
testParagraph.innerText = "This is a paragraph"

/** Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль.
 * Кожному з елементів присвоїти новий клас nav-item.*/

const mainHeader = document.querySelector(".main-header")
// console.log(mainHeader.children);

const childElem = mainHeader.children
console.log(childElem);
for (const childElement of childElem) {
    childElement.classList.add("nav-item")
}

    /**найти всі елементи із класом container. Видалити цей клас у цих елементів.*/
const deletElem = document.querySelectorAll(".container")
for (const deletElements of deletElem) {
    deletElements.classList.remove("container")
}




