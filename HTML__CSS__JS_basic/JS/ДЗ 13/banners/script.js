/**
 *
 *
1.Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
2.Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
3.Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?*/

//Відповідь
// 1.setTimeout визиває функцію один раз через якийсь проміжуток часу , а setInterval запускає футкцкію періодично через якийсь інтервал часу
// 2.Функція setTimeout() з нульовою затримкою призначена для того щоб визвати функцію якомога скоріше але вона буде визвана
//  після того як закінчиться виконання поточного кода
// 3.Щоб остановити виконання функції setInterval  ми використовуємо clearInterval


document.body.insertAdjacentHTML("beforeend",'<button class = "button-stop">Припинити</button>')
document.body.insertAdjacentHTML('beforeend', '<button class = "button-start" >Відновити показ</button>');
const buttonStop = document.querySelector(".button-stop")
const buttonStart = document.querySelector(".button-start")

let counter = 1;

const imgChenge = () => {

    const imageAdd = setInterval(()=> {
        if (counter > 4) {
            counter = 1;
        }
        document.querySelector('.image-to-show').src = `./img/${counter}.jpg`;
        counter++;
    },1000)
    buttonStop.addEventListener("click",() => {
        clearInterval(imageAdd);
    })
}
imgChenge()
buttonStart.addEventListener("click",() => {
    imgChenge()
})

